from __future__ import print_function
import numpy as np
import numexpr as ne
import timeit


N = 2**14
NUMBER = 5
SETUP = "from __main__ import np, ne, a"


def run(s):
    print("Testing {}...".format(s))
    t = timeit.timeit(s, setup=SETUP, number=NUMBER)
    print("time [s] = {0:.4f}".format(t/NUMBER))


if __name__ == '__main__':
    print("Allocating random Numpy array 'a' of size ({}, {})".format(N, N))
    a = np.random.rand(N, N)
    run("np.multiply(a, a)")
    run("ne.evaluate('a*a')")
    if np.array_equal(np.multiply(a, a), ne.evaluate('a*a')):
        print("Both give same results.")
    else:
        print("results are different!")
