rm -rf openblas
virtualenv openblas
source openblas/bin/activate
cd openblas
mkdir download
mkdir build
cd download
wget https://github.com/numpy/numpy/releases/download/v1.14.0/numpy-1.14.0.tar.gz
cd ../build/
tar xf ../download/numpy-1.14.0.tar.gz 
cd numpy-1.14.0
cat > site.cfg <<EOL
[default]
library_dirs= /usr/lib/openblas-base
 
[atlas]
atlas_libs = openblas
EOL
python3 setup.py install

