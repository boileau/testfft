"""
Uncomment below to use 32 bit floats,
increasing the speed by a factor of 4
and remove the difference between the "builders" and "FFTW" methods
"""
from __future__ import print_function
import functools
from timeit import default_timer
import numpy
import pyfftw
import scipy.fftpack

NTHREAD = 4
#a = numpy.random.rand(2364,2756).astype('complex128')
#a = numpy.random.rand(2364,2756).astype('complex64')
n = 8192
a = numpy.random.rand(n, n).astype('complex64')


def full_name(o):
    try:
        return "{}.{}".format(o.__module__, o.__name__)
    except AttributeError:
        return "Unknown"


def timer(func, comment=None):
    """Decorator that display the function execution time"""
    @functools.wraps(func)
    def func_call(*args, **kwargs):
        if comment:
            print("Running {} with {}...".format(full_name(func), comment))
        else:
            print("Running {}...".format(full_name(func)))
        start_time = default_timer()  # Start timer
        result = func(*args, **kwargs)
        elapsed_time = default_timer() - start_time
        print("Total time [s]: {:f}".format(elapsed_time))
        return result
    return func_call


if __name__ == '__main__':

    print("Running with a of size =", a.shape)

    # Numpy FFT
    numpy.fft.fft2 = timer(numpy.fft.fft2)
    b1 = numpy.fft.fft2(a)

    # Scipy FFT
    scipy.fftpack.fft2 = timer(scipy.fftpack.fft2)
    b2 = scipy.fftpack.fft2(a)

    # PyFFTW
    pyfftw.interfaces.scipy_fftpack.fft2 = timer(
            pyfftw.interfaces.scipy_fftpack.fft2)

    # PyFFTW 2
    pyfftw.forget_wisdom()
    pyfftw.interfaces.numpy_fft.fft2 = timer(pyfftw.interfaces.numpy_fft.fft2)
    b4 = pyfftw.interfaces.numpy_fft.fft2(a, threads=NTHREAD)

    # PyFFTW 3
    pyfftw.forget_wisdom()
    b4 = numpy.zeros_like(a)
    fftw_measure = timer(pyfftw.FFTW, comment="FFTW_MEASURE")
    b5 = fftw_measure(a, b4, axes=(0, 1), direction='FFTW_FORWARD',
                      flags=('FFTW_MEASURE', ), threads=NTHREAD,
                      planning_timelimit=None)

    # PyFFTW 3
    pyfftw.forget_wisdom()
    fftw_estimate = timer(pyfftw.FFTW, comment="FFTW_ESTIMATE")
    b6 = fftw_estimate(a, b4, axes=(0, 1), direction='FFTW_FORWARD',
                       flags=('FFTW_ESTIMATE', ), threads=NTHREAD,
                       planning_timelimit=None)

    # PyFFTW 4
    pyfftw.forget_wisdom()
    pyfftw_builders_measure = timer(pyfftw.builders.fft2,
                                    comment="FFTW_MEASURE")
    b7 = pyfftw_builders_measure(a, s=None, axes=(-2, -1),
                                 overwrite_input=False,
                                 planner_effort='FFTW_MEASURE',
                                 threads=NTHREAD, auto_align_input=False,
                                 auto_contiguous=False, avoid_copy=True)

    # PyFFTW 5
    pyfftw.forget_wisdom()
    pyfftw_builders_estimate = timer(pyfftw.builders.fft2,
                                     comment="FFTW_ESTIMATE")
    b8 = pyfftw_builders_estimate(a, s=None, axes=(-2, -1),
                                  overwrite_input=False,
                                  planner_effort='FFTW_ESTIMATE',
                                  threads=NTHREAD, auto_align_input=False,
                                  auto_contiguous=False, avoid_copy=True)

